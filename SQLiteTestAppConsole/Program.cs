﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace SQLiteTestAppConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //Connection();

            EF();
            
        }

        private static void EF()
        {
            TestbaseEntities tb = new TestbaseEntities();

            Product prod1 = new Product() { Name = "Magla", RowPosition = 1, UnitPrice = 10 };
            tb.Products.Add(prod1);

            Transaction_Product tp = new Transaction_Product();
            tp.Product = prod1;
            tp.TransactionID = 1;
            tp.Quantity = 10;
            tb.Transaction_Product.Add(tp);

            tb.SaveChanges();


            foreach (Product prod in tb.Products)
            {
                Console.WriteLine("ID: {0}  Name: {1}  UnitPrice: {2}", prod.ProductID, prod.Name, prod.UnitPrice);
            }

            Console.Write("\n\n\n");

            foreach (Transaction_Product tpx in tb.Transaction_Product)
            {
                Console.WriteLine("Name: {0}  TransID: {1}  Quantity: {2}", tpx.Product.Name, tpx.TransactionID, tpx.Quantity);
            }

            Console.Write("\n\n\n\n");

            foreach (Transaction_Product tpx in prod1.Transaction_Product)
            {
                Console.WriteLine("Name: {0}  TransID: {1}  Quantity: {2}", tpx.Product.Name, tpx.TransactionID, tpx.Quantity);
            }


        }

        private static void Connection()
        {
            SQLiteConnection myconn = new SQLiteConnection("Data Source=testbase.db");
            myconn.Open();

            string selcom = "SELECT * FROM PRODUCT";
            SQLiteCommand comm = new SQLiteCommand(selcom, myconn);

            SQLiteDataReader reader = comm.ExecuteReader();

            while (reader.Read())
                Console.WriteLine("ID: {0}  Name: {1} UnitPrice: {2} ",
                    reader["ProductID"],
                    reader["Name"],
                    reader["UnitPrice"]);

            string insertcomm = "INSERT INTO Product(ProductID, Name, UnitPrice, RowPosition) VALUES (NULL,'P5',10,1)";
            SQLiteCommand commInsert = new SQLiteCommand(insertcomm, myconn);
            commInsert.ExecuteNonQuery();
        }
    }
}
